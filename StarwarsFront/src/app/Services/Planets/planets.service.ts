import { Planet } from './../../Models/Planet';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  readonly baseUrl: string = environment.baseUrl+'planets'

  constructor(private httpClient: HttpClient) { }

  getList() :Promise<Planet[]> {
    return this.httpClient.get<Planet[]>(this.baseUrl).toPromise();
  }

  getById(id) :Promise<Planet>{
    return this.httpClient.get<Planet>(this.baseUrl+'/'+id).toPromise();
  }
}
